var correct = "1,2,3,4,5,6,7,8,9";

function checkRows (array) {
  var valid = true;
  array.forEach(function(row) {
    if (row.sort().toString() !== correct) {
      valid = false;
    }
  });
  return valid
}

function checkCols (array) {
  var colArray = [];
  array.forEach(function(col) {
    colArray.push(col[0]);
  });
  return checkRows([colArray]);
}

function checkSudoku (array) {
  return (
    checkRows(array) && 
    checkCols(array)
  );
}

var correctArray = [
  [4,5,6,7,8,9,1,2,3],
  [7,8,9,2,3,1,4,5,6],
  [2,1,4,5,6,8,9,3,7],
  [3,6,5,9,2,7,8,1,4],
  [8,9,7,1,4,3,2,6,5],
  [5,7,2,3,9,4,6,8,1],
  [9,4,8,6,1,5,3,7,2],
  [6,3,1,8,7,2,5,4,9]
];

document.write(checkSudoku(correctArray));
